import Main.lineSink
import akka.NotUsed
import akka.actor.ActorSystem
import akka.stream.scaladsl.{Sink, Source}
import akka.stream.{ActorMaterializer, IOResult}

import scala.concurrent.{ExecutionContextExecutor, Future}
import scala.util.Try


val path = "/home/mikhail/IdeaProjects/BioHack2019/csv/summary.txt"
val countedFilteredValues = scala.io.Source.fromFile(path)
  .getLines()
  .toList

countedFilteredValues
  .groupBy(w => w)
  .mapValues(_.size)

countedFilteredValues.foreach(println)

val r = countedFilteredValues
  .map(_.toLowerCase())
  .map(_.replaceAll(":", ""))
  .filter(e => Try(Integer.parseInt(e)).isFailure)
  .distinct


val fullGsm = scala.io.Source.fromFile("/home/mikhail/IdeaProjects/BioHack2019/csv/GSM_gsm, organism_ch1, molecule_ch1, molecule_ch2, characteristics_ch1, characteristics_ch2, description_plus_inner_fields_SELECT_gsm_organism_ch1_molecule_ch1_molecule_ch2_characteristic_201903020543.csv")
  .getLines()
  .toList

r.size

//r.foreach(println)
implicit val system: ActorSystem = ActorSystem("QuickStart")
implicit val ec: ExecutionContextExecutor = system.dispatcher
implicit val materializer: ActorMaterializer = ActorMaterializer()



val pathOutput = "/home/mikhail/IdeaProjects/BioHack2019/csv/result_summary/"
val keysSink: Sink[String, Future[IOResult]] = lineSink(pathOutput)


////var array = Array.ofDim[Int](fullGsm.size, countedFilteredValues.size)
////var array = Array.ofDim[Int](4000, countedFilteredValues.size)
//var array = Array.ofDim[Int](2, 2)
//
////for (i <- fullGsm.indices) {
////for (i <- 0 until 4000) {
//for (i <- 0 until 2) {
//  //  for (j <- countedFilteredValues.indices) {
//  for (j <- 0 until 2) {
//    if (fullGsm(i).contains(countedFilteredValues(j))) {
//      //    if (i + j % 2 == 0) {
//      array(i)(j) = 1
//      scala.util.control.Breaks.breakable {
//        scala.util.control.Breaks.break()
//      }
//    }
//    else {
//      array(i)(j) = 0
//    }
//
//  }
//}
//
//array.map(_.mkString(";")).toList

val sourceStrings1 = countedFilteredValues
val source1: Source[String, NotUsed] = Source(sourceStrings1)
source1.runWith(keysSink).onComplete(_ => system.terminate())
//        source1.runWith(keysSink).onComplete(_ => system.terminate())






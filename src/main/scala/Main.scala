import java.nio.file.Paths

import akka.NotUsed
import akka.actor.ActorSystem
import akka.stream.scaladsl.{FileIO, Flow, Keep, Sink, Source}
import akka.stream.{ActorMaterializer, IOResult}
import akka.util.ByteString

import scala.concurrent.{ExecutionContextExecutor, Future}
import scala.util.Try

object Main extends App {

  implicit val system: ActorSystem = ActorSystem("QuickStart")
  implicit val ec: ExecutionContextExecutor = system.dispatcher
  implicit val materializer: ActorMaterializer = ActorMaterializer()

  private val baseUrl = "/home/mikhail/IdeaProjects/BioHack2019/csv/"
  private val fileNames = List(
    //        "distinct_characteristics_ch1_FROM_gsm.csv"
    //        "distinct_characteristics_ch2_FROM_gsm.csv"
    //        "distinct_description_FROM_gsm.csv"
    //        "characteristics_ch1_FROM_gsm.csv"
    //    "characteristics_ch2_FROM_gsm.csv"
    "summary.txt"
  )

  //  private val fileName = "distinct_description_FROM_gsm.csv"
  for (fileName <- fileNames) {
    val fullSourcePath = baseUrl + "source/" + fileName
    val pairsSinkPath = s"${baseUrl}pairs/PAIRS_for_${fileName}"
    val countsSinkPath = s"${baseUrl}counts/COUNTS_for_${fileName}"
    val keysSink: Sink[String, Future[IOResult]] = lineSink(pairsSinkPath)
    val countsSink: Sink[String, Future[IOResult]] = lineSink(countsSinkPath)
    //
    val sourceStrings = scala.io.Source.fromFile(fullSourcePath)
      .getLines()
      .map(_.replaceAll(";", "\n"))
      .map(_.replaceAll("\"", ""))
      .map(_.replaceAll("\t", ""))
      .toList

    val source: Source[String, NotUsed] = Source(sourceStrings)
    source.runWith(keysSink)
    //.onComplete(_ => system.terminate())

    Thread.sleep(1000)

    val pairsSourceStrings = scala.io.Source.fromFile(pairsSinkPath)
      .getLines()
      .toList

    //  pairsSourceStrings.distinct.foreach(println)

    val values = pairsSourceStrings
    //      .map(_.dropWhile(e => e != ':'))
    //      .map(_.replaceAll(": ", ""))

    val countedFilteredValues = values
      //      .groupBy(w => w)
      //      .mapValues(_.size)
      //      .filter(_._2 > 100)
      .map(_.replaceAll(":", ""))
      .map(_.toLowerCase())
      .map(_.replaceAll("--", ""))
      .map(_.replaceAll("%", ""))
      .filter(e => Try(Integer.parseInt(e)).isFailure)
      .map(_.toString())
      .toList
      .distinct


    pairsSourceStrings.distinct.foreach(println)
    //  values.foreach(println)
    countedFilteredValues.foreach(println)
    //  println(values.size)
    //  println(values.distinct.size)

    val sourceStrings1 = countedFilteredValues
    val source1: Source[String, NotUsed] = Source(sourceStrings1)
    source1.runWith(countsSink).onComplete(_ => system.terminate())

  }

  def lineSink(filename: String): Sink[String, Future[IOResult]] = Flow[String]
    .map(s ⇒ ByteString(s + "\n"))
    .toMat(FileIO.toPath(Paths.get(filename)))(Keep.right)

}
